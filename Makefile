.DEFAULT_GOAL := help

ifndef CONDA_PREFIX
$(error Conda not active, please install conda and then create an environment with `conda create --name one-earth-2022` and activate it with `conda activate one-earth-2022`))
else
ifeq ($(CONDA_DEFAULT_ENV),base)
$(error Do not install to conda base environment. Source a different conda environment e.g. `conda activate one-earth-2022` or `conda create --name one-earth-2022` and rerun make))
endif
VENV_DIR=$(CONDA_PREFIX)
endif

# use mamba if available
MAMBA_EXE := $(shell command -v mamba 2> /dev/null)
ifndef MAMBA_EXE
MAMBA_OR_CONDA=$(CONDA_EXE)
else
MAMBA_OR_CONDA=$(MAMBA_EXE)
endif

PYTHON=$(CONDA_PREFIX)/bin/python
CONDA_ENV_FILE=environment.yml


define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([\$$\(\)a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT


help:  ## print short description of each target
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

test: $(CONDA_PREFIX)  ## run tests of utils
	pytest -r a -vv tests

.PHONY: format
format:  ## re-format files
	make isort
	make black

.PHONY: black
black: $(VENV_DIR)  ## apply black formatter to source and tests
	@status=$$(git status --porcelain src tests docs scripts); \
	if test ${FORCE} || test "x$${status}" = x; then \
		$(VENV_DIR)/bin/black --exclude _version.py setup.py src tests; \
	else \
		echo Not trying any formatting. Working directory is dirty ... >&2; \
	fi;

.PHONY: conda-environment
conda-environment: $(CONDA_PREFIX)  ## create conda environment for running the notebooks
$(CONDA_PREFIX): $(CONDA_ENV_FILE) setup.py
	$(MAMBA_OR_CONDA) env update --name $(CONDA_DEFAULT_ENV) -f $(CONDA_ENV_FILE)
	$(VENV_DIR)/bin/pip install -e .
	$(VENV_DIR)/bin/pip install black
	touch $(CONDA_PREFIX)
