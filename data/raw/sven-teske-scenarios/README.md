# Teske scenarios

These scenarios were kindly provided by Sven Teske et al. from UTS. Their work is published in 

> *Achieving the Paris Climate Agreement Goals: (part 2) Science-based Target Setting for the Finance industry — Net-Zero Sectoral 1.5˚C Pathways for Real Economy Sectors.* ISBN 978-3-030-99176-0.

All of their scenario data, including the file here, is publicly available at https://www.uts.edu.au/isf/news/how-limit-global-warming-1.5degc-guidance-sectors.
We thank these authors again for providing us with their scenario data and encourage those with an interest in such scenarios to explore the website linked in the previous sentence in more detail.
