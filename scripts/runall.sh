#!/usr/bin/env bash
# Rerun all notebooks

# Stop on first error
set -e

NOTEBOOKS=(
  010_crunch-areas.ipynb
  110_crunch-pathways.ipynb
  140_harmonise-and-clean-sr15-database.ipynb
  145_calculate-sr15-afolu-emissions-without-model-reported-sinks.ipynb
  150_create-scenarios.ipynb
  155_infill-montreal-halogens.ipynb
  200_run-magicc.ipynb
  210_process-magicc-output.ipynb
  400_plot-landuse-pathways-and-calculate-pathway-stats.ipynb
  401_plot-landuse-pathways-r5-regions.ipynb
  402_plot-magicc-temperatures.ipynb
  403_plot-carbon-fluxes.ipynb
  404_clean-specs.ipynb
  500_summary-excel.ipynb
)

for nb in "${NOTEBOOKS[@]}"
do
  jupyter nbconvert --to notebook --execute notebooks/$nb --output=$nb --ExecutePreprocessor.timeout=-1
done
