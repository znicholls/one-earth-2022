#!/bin/bash
#
# Upload the repository to Zenodo
#

if [ -f .env ]
then
  # thank you https://stackoverflow.com/a/20909045
  echo "Reading .env file"
  export $(grep -v '^#' .env | xargs)
fi

PROTOCOL_METADATA_FILE="one-earth-2022-metadata.json"
DEPOSITION_ID_OLD="5571117"

DEPOSITION_ID=$(openscm-zenodo create-new-version "${DEPOSITION_ID_OLD}" "${PROTOCOL_METADATA_FILE}" --zenodo-url zenodo.org)
echo "DEPOSITION_ID = ${DEPOSITION_ID}"

BUCKET_ID=$(openscm-zenodo get-bucket ${DEPOSITION_ID} --zenodo-url zenodo.org)
echo "BUCKET_ID = ${BUCKET_ID}"


# echo "Would upload"
# git ls-tree -r HEAD --name-only | xargs -L1 echo

echo "Uploading"
git ls-tree -r HEAD --name-only | xargs -L1 -i openscm-zenodo upload {} "${BUCKET_ID}" --zenodo-url zenodo.org --root-dir "${ROOT_DIR_TO_STRIP}"