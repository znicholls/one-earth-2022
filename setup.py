import versioneer
from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


PACKAGE_NAME = "one_earth"

DESCRIPTION = "Code for use in the `one-earth-2022` repository"

SOURCE_DIR = "src"


class Utils(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest

        pytest.main(self.test_args)


cmdclass = versioneer.get_cmdclass()
cmdclass.update({"test": Utils})


setup(
    version=versioneer.get_version(),
    cmdclass=cmdclass,
    name=PACKAGE_NAME,
    description=DESCRIPTION,
    packages=find_packages(SOURCE_DIR),  # no exclude as only searching in `src`
    package_dir={"": SOURCE_DIR},
)
