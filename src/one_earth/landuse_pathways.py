import numpy as np
import scipy.interpolate
import scmdata
from openscm_units import unit_registry

from .errors import MaximumCarbonExceededError


def _get_mags(
    x_units,
    phase_in_period,
    saturation_period,
    phase_out_period,
    check_saturation_period=True,
):
    phase_in_period_mag = phase_in_period.to(x_units).magnitude
    saturation_period_mag = saturation_period.to(x_units).magnitude
    phase_out_period_mag = phase_out_period.to(x_units).magnitude

    to_check = [
        ("phase_in_period", phase_in_period_mag),
        ("phase_out_period", phase_out_period_mag),
    ]
    if check_saturation_period:
        to_check.append(("saturation_period", saturation_period_mag))

    for name, mag in to_check:
        if mag < 1:
            raise ValueError(
                "`{}` must be greater than or equal to 1 year".format(name)
            )

    return phase_in_period_mag, saturation_period_mag, phase_out_period_mag


def _create_pathway(
    max_rate,
    saturation_period,
    phase_in_period,
    phase_out_period,
    start_year,
    out_years,
    model,
    scenario,
    variable,
    region,
    check_saturation_period=True,
):
    x_units = "yr"

    phase_in_period_mag, saturation_period_mag, phase_out_period_mag = _get_mags(
        x_units,
        phase_in_period,
        saturation_period,
        phase_out_period,
        check_saturation_period=check_saturation_period,
    )

    y_units = "MgC / yr"
    max_rate_mag = max_rate.to(y_units).magnitude

    phase_out_year = (
        start_year + phase_in_period_mag + saturation_period_mag + phase_out_period_mag
    )

    if phase_out_year >= out_years[-1]:
        raise ValueError(
            "The `phase_out_year` is {} which is greater than or equal to the "
            "last year in `out_years` which is {}, extend `out_years`".format(
                phase_out_year,
                out_years[-1],
            )
        )

    x = [
        out_years[0],
        start_year,
        start_year + phase_in_period_mag,
    ]
    y = [0, 0, max_rate_mag]
    if saturation_period_mag > 0:
        x.append(start_year + phase_in_period_mag + saturation_period_mag)
        y.append(max_rate_mag)

    x += [phase_out_year, out_years[-1]]
    y += [0, 0]

    interpolator = scipy.interpolate.PchipInterpolator(x, y, extrapolate=False)
    pathway = interpolator(out_years)

    out = scmdata.ScmRun(
        data=pathway,
        index=out_years,
        columns={
            "model": model,
            "scenario": scenario,
            "variable": variable,
            "unit": y_units,
            "region": region,
        },
    )

    return out


def _get_max_total_uptake(inp):
    total_uptake = inp.integrate()
    max_total_uptake = total_uptake.values.max()
    total_uptake_units = total_uptake.get_unique_meta("unit", True)
    max_total_uptake = max_total_uptake * unit_registry(total_uptake_units)

    return max_total_uptake


def _cut_out_years(inscmrun, integrate_years, out_years):
    # don't want to drop the last year, only required to calculate the full integral
    drop_years = integrate_years[:-1]

    out = inscmrun.copy()

    new_up_values = out.filter(
        year=range(out_years[0], drop_years[0])
    ).values.squeeze()
    new_down_values = out.filter(
        year=range(drop_years[-1] + 1, out_years[-1])
    ).values.squeeze()
    new_values = np.concatenate([new_up_values, new_down_values])
    new_values = np.concatenate(
        [new_values, np.zeros(len(out_years) - len(new_values))]
    )

    out.values[:] = new_values

    return out


def create_landuse_pathway(
    area,
    max_rate_per_area,
    max_total_per_area,
    saturation_period,
    phase_in_period,
    phase_out_period,
    start_year,
    out_years,
    model,
    scenario,
    variable,
    region,
    raise_if_greater_than_max=True,
):
    """
    Create land-use pathway

    Parameters
    ----------
    area : :class:`pint.Quantity`
        Area of land which can be used for the pathway

    max_rate_per_area : :class:`pint.Quantity`
        Maximum rate of emissions possible for this pathway

    max_total_per_area : :class:`pint.Quantity`
        Maximum total (i.e. cumulative) emissions from this pathway

    saturation_period : :class:`pint.Quantity`
        Time this pathway can be at its maximum emissions

    phase_in_period : :class:`pint.Quantity`
        Time this pathway takes to reach its maximum emissions from zero

    phase_out_period : :class:`pint.Quantity`
        Time this pathway takes to reach zero emissions from its maximum

    start_year : int
        Year in which the pathway should begin to emit/uptake carbon

    out_years : :class:`np.ndarray`
        The years which should be included in the output

    model : str
        Model metadata for this pathway

    scenario : str
        Scenario metadata for this pathway

    variable : str
        Variable metadata for this pathway

    region : str
        Region metadata for this pathway

    raise_if_greater_than_max : bool
        Should an error be raised if the total carbon emissions is exceeded? If ``False``, the saturation period will be shortened until the total carbon uptake is equal to the maximum.

    Returns
    -------
    :class:`scmdata.ScmRun`
        Generated pathway

    Raises
    ------
    MaximumCarbonExceededError
        If ``raise_if_greater_than_max is True`` and the maximum amount of carbon is exceeded

    ValueError
        ``phase_in_period``, ``saturation_period`` or ``phase_out_period`` are less than one year

    Notes
    -----
    It is up to the user to decide the conventions around whether positive
    values imply uptake or release of carbon.
    """
    max_rate = max_rate_per_area * area

    out = _create_pathway(
        max_rate,
        saturation_period,
        phase_in_period,
        phase_out_period,
        start_year,
        out_years,
        model,
        scenario,
        variable,
        region,
    )

    max_total_uptake = _get_max_total_uptake(out)
    max_allowable_uptake = area * max_total_per_area

    if max_total_uptake > max_allowable_uptake:
        if raise_if_greater_than_max:
            error_msg = (
                "Maximum carbon to be taken up is {}. "
                "Carbon taken up according to pathway is {}.".format(
                    max_allowable_uptake.to(max_total_uptake.units),
                    max_total_uptake,
                )
            )
            raise MaximumCarbonExceededError(error_msg)

        # otherwise shorten saturation period
        excess_uptake = max_total_uptake - max_allowable_uptake
        required_shortening = excess_uptake / max_rate
        new_saturation_period = np.floor(saturation_period - required_shortening)

        if new_saturation_period.to("yr").magnitude < 1:
            out = _create_pathway(
                max_rate,
                0 * saturation_period.units,
                phase_in_period,
                phase_out_period,
                start_year,
                out_years,
                model,
                scenario,
                variable,
                region,
                check_saturation_period=False,
            )

            max_uptake_year = int(
                out.timeseries(time_axis="year").idxmax(axis=1).values.squeeze()
            )

            max_total_uptake = _get_max_total_uptake(out)
            excess_uptake = max_total_uptake - max_allowable_uptake

            def get_integrate_years(i):
                return range(
                    # -1 so we go back one step in time even if i = 0
                    max(max_uptake_year - i - 1, start_year),
                    # + 2 to cover
                    #   a) range requires + 1 to include the last year,
                    #   b) we integrate over the whole of the last year which we might drop
                    max_uptake_year + i + 1 + 1,
                )

            def get_cut_uptake(original, integrate_yrs):
                integrated = original.filter(year=integrate_yrs).integrate()

                return integrated.values.squeeze()[-1] * unit_registry(
                    integrated.get_unique_meta("unit", True)
                )

            i = 0
            integrate_years = get_integrate_years(i)
            cut_uptake = get_cut_uptake(out, integrate_years)

            while cut_uptake <= excess_uptake:
                i += 1
                integrate_years = get_integrate_years(i)
                cut_uptake = get_cut_uptake(out, integrate_years)

            out = _cut_out_years(out, integrate_years, out_years)
            max_total_uptake = _get_max_total_uptake(out)

            if max_total_uptake > max_allowable_uptake:
                # drop one more year (should be edge case at this point)
                integrate_years = get_integrate_years(i + 1)
                out = _cut_out_years(out, integrate_years, out_years)

                # final check
                max_total_uptake = _get_max_total_uptake(out)
                if max_total_uptake > max_allowable_uptake:
                    raise ValueError(
                        "Shouldn't happen"
                    )  # pragma: no cover # emergency valve

        else:
            out = create_landuse_pathway(
                area,
                max_rate_per_area,
                max_total_per_area,
                new_saturation_period,
                phase_in_period,
                phase_out_period,
                start_year,
                out_years,
                model,
                scenario,
                variable,
                region,
                raise_if_greater_than_max=False,
            )

    return out
