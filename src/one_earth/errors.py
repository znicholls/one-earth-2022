class MaximumCarbonExceededError(ValueError):
    """
    More carbon is taken up by a pathway than is available
    """

    pass
