# One Earth 2022

|  Info   |Badge|
|---------|-----|
| Article | [![One Earth](https://img.shields.io/badge/One%20Earth-Dooley%20et%20al.%20(2022)-brightgreen)](https://doi.org/10.1016/j.oneear.2022.06.002) |
| Archive | [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5571116.svg)](https://doi.org/10.5281/zenodo.5571116) |
| License | TBD |

Code and data to create the figures in the publication Dooley et al., 2022 on the potential uptake from land management.
If this repository is of use to you, please cite

> Dooley *et al.* 2022, *One Earth* [https://doi.org/10.1016/j.oneear.2022.06.002]()

All code and data has also been archived at Zenodo (https://doi.org/10.5281/zenodo.5571116).

If you have any issues, please raise them in the [issue tracker](<https://gitlab.com/znicholls/one-earth-2022/issues>).

## Installation

### Required packages

The simplest way to get setup is using conda or mamba. Once you have conda or mamba installed, you
can simply run the following steps.

```sh
# create a new conda environment
mamba create --name one-earth-2022
# activate it
mamba activate one-earth-2022
# install dependencies
mamba env update --name one-earth-2022 -f environment.yml
# install pip dependencies and local package
pip install -e .
```

### Getting MAGICC7

MAGICC7 can be downloaded using the script `scripts/get-magicc.sh`. 
Instructions for how to use this script are included in the script.

MAGICC7 and the parameter set are available from www.magicc.org. 
After registering, you will be sent personal links for downloading. 
These links are linked to your magicc.org account and can be used to download MAGICC.

Please read the license and expectations of using MAGICC carefully (available at https://magicc.org/download/magicc7), we rely on users to act in a way which brings both new scientific outcomes but also acknowledges the work put into the MAGICC AR6 setup.

## Running the analysis

The analysis is all documented in notebooks. 
Simply run the notebooks in order or use the script `scripts/runall.sh`.
Note that this requires some computing power to do all the runs.
