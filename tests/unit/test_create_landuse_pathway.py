import numpy as np
import pint.errors
import pytest
import scmdata
import scmdata.testing as scmdt
import scipy.interpolate
import re
from openscm_units import unit_registry

from one_earth.errors import MaximumCarbonExceededError
from one_earth.landuse_pathways import create_landuse_pathway

# handles run off end of time properly (i.e. out_years ends before phase out
# period ends)
# start_year == first value in out_years

tmodel_scenario_variable_region = pytest.mark.parametrize(
    "tmodel,tscenario,tvariable,tregion",
    (
        ("test", "blip", "Emissions|CO2|Reforestation", "World"),
        ("t", "blip", "Emissions|CO2|Reforestation", "World|R5ASIA"),
        ("test", "b", "Emissions|CO2|Afforestation", "World|R6.2MAF"),
    ),
)


@tmodel_scenario_variable_region
def test_create_pathway_single_year_blip(tmodel, tscenario, tvariable, tregion):
    tarea = 1.0 * unit_registry("ha")
    tmax_rate_per_area = 1.0 * unit_registry("MgC / ha / yr")
    tmax_total_per_area = 100 * unit_registry("MgC / ha")
    tsaturation_period = 1 * unit_registry("yr")
    tphase_in_period = 1 * unit_registry("yr")
    tphase_out_period = 1 * unit_registry("yr")
    tstart_year = 2020
    tout_years = np.arange(1850, 2300 + 1)

    exp_dat = np.zeros_like(tout_years)
    start_year_idx = tstart_year - tout_years[0]
    exp_dat[start_year_idx + 1 : start_year_idx + 3] = 1

    exp = scmdata.ScmRun(
        data=exp_dat,
        index=tout_years,
        columns={
            "model": tmodel,
            "scenario": tscenario,
            "variable": tvariable,
            "unit": "MgC / yr",
            "region": tregion,
        },
    )

    res = create_landuse_pathway(
        area=tarea,
        max_rate_per_area=tmax_rate_per_area,
        max_total_per_area=tmax_total_per_area,
        saturation_period=tsaturation_period,
        phase_in_period=tphase_in_period,
        phase_out_period=tphase_out_period,
        start_year=tstart_year,
        out_years=tout_years,
        model=tmodel,
        scenario=tscenario,
        variable=tvariable,
        region=tregion,
    )

    scmdt.assert_scmdf_almost_equal(res, exp)


@tmodel_scenario_variable_region
def test_create_pathway_two_year_blip(tmodel, tscenario, tvariable, tregion):
    tarea = 1.0 * unit_registry("ha")
    tmax_rate_per_area = 1.0 * unit_registry("MgC / ha / yr")
    tmax_total_per_area = 100 * unit_registry("MgC / ha")
    tsaturation_period = 2 * unit_registry("yr")
    tphase_in_period = 1 * unit_registry("yr")
    tphase_out_period = 1 * unit_registry("yr")
    tstart_year = 2020
    tout_years = np.arange(1850, 2300 + 1)

    exp_dat = np.zeros_like(tout_years)
    start_year_idx = tstart_year - tout_years[0]
    exp_dat[start_year_idx + 1 : start_year_idx + 4] = 1

    exp = scmdata.ScmRun(
        data=exp_dat,
        index=tout_years,
        columns={
            "model": tmodel,
            "scenario": tscenario,
            "variable": tvariable,
            "unit": "MgC / yr",
            "region": tregion,
        },
    )

    res = create_landuse_pathway(
        area=tarea,
        max_rate_per_area=tmax_rate_per_area,
        max_total_per_area=tmax_total_per_area,
        saturation_period=tsaturation_period,
        phase_in_period=tphase_in_period,
        phase_out_period=tphase_out_period,
        start_year=tstart_year,
        out_years=tout_years,
        model=tmodel,
        scenario=tscenario,
        variable=tvariable,
        region=tregion,
    )

    scmdt.assert_scmdf_almost_equal(res, exp)


def _create_landuse_pathway(x, y, x_out, model, scenario, variable, unit, region):
    exp_dat = scipy.interpolate.PchipInterpolator(x, y, extrapolate=False)(x_out)

    return scmdata.ScmRun(
        data=exp_dat,
        index=x_out,
        columns={
            "model": model,
            "scenario": scenario,
            "variable": variable,
            "unit": unit,
            "region": region,
        },
    )


def _create_landuse_pathway_scmrun(
    yunit,
    xunit,
    max_rate_per_area,
    area,
    saturation_period,
    phase_in_period,
    phase_out_period,
    start_year,
    out_years,
    model,
    scenario,
    variable,
    region,
):
    max_flux_mag = (max_rate_per_area * area).to(yunit).magnitude
    ybase = [
        0,
        0,
        max_flux_mag,
        max_flux_mag,
        0,
        0,
    ]

    max_rate_in = start_year + phase_in_period.to(xunit).magnitude
    end_saturation = max_rate_in + saturation_period.to(xunit).magnitude
    back_to_zero = end_saturation + phase_out_period.to(xunit).magnitude

    xbase = [
        out_years[0],
        start_year,
        max_rate_in,
        end_saturation,
        back_to_zero,
        out_years[-1],
    ]

    return _create_landuse_pathway(
        xbase,
        ybase,
        out_years,
        model,
        scenario,
        variable,
        yunit,
        region,
    )


@pytest.mark.parametrize(
    "tarea", (1.0 * unit_registry("ha"), 0.5 * unit_registry("Mha"))
)
@pytest.mark.parametrize(
    "tmax_rate_per_area",
    (10.0 * unit_registry("GgC / ha / yr"), 3.2 * unit_registry("MgC / ha / yr")),
)
@pytest.mark.parametrize("tmax_total_per_area", (10 ** 6 * unit_registry("GgC / ha"),))
@pytest.mark.parametrize(
    "tsaturation_period", (10 * unit_registry("yr"), 20 * unit_registry("yr"))
)
@pytest.mark.parametrize(
    "tphase_in_period", (2 * unit_registry("yr"), 50 * unit_registry("yr"))
)
@pytest.mark.parametrize(
    "tphase_out_period", (2 * unit_registry("yr"), 50 * unit_registry("yr"))
)
@pytest.mark.parametrize("tstart_year", (1860, 1950))
@pytest.mark.parametrize(
    "tout_years", (np.arange(1850, 2100 + 1), np.arange(1850, 2300 + 1))
)
@tmodel_scenario_variable_region
@pytest.mark.slow
def test_create_pathway(
    tarea,
    tmax_rate_per_area,
    tmax_total_per_area,
    tsaturation_period,
    tphase_in_period,
    tphase_out_period,
    tstart_year,
    tout_years,
    tmodel,
    tscenario,
    tvariable,
    tregion,
):
    tyunit = "tC / yr"
    txunit = "yr"

    exp = _create_landuse_pathway_scmrun(
        yunit=tyunit,
        xunit=txunit,
        max_rate_per_area=tmax_rate_per_area,
        area=tarea,
        saturation_period=tsaturation_period,
        phase_in_period=tphase_in_period,
        phase_out_period=tphase_out_period,
        start_year=tstart_year,
        out_years=tout_years,
        model=tmodel,
        scenario=tscenario,
        variable=tvariable,
        region=tregion,
    )

    res = create_landuse_pathway(
        area=tarea,
        max_rate_per_area=tmax_rate_per_area,
        max_total_per_area=tmax_total_per_area,
        saturation_period=tsaturation_period,
        phase_in_period=tphase_in_period,
        phase_out_period=tphase_out_period,
        start_year=tstart_year,
        out_years=tout_years,
        model=tmodel,
        scenario=tscenario,
        variable=tvariable,
        region=tregion,
    ).convert_unit(tyunit)

    scmdt.assert_scmdf_almost_equal(res, exp)


@pytest.fixture
def tkwargs():
    return dict(
        area=1.0 * unit_registry("ha"),
        max_rate_per_area=1.0 * unit_registry("MgC / ha / yr"),
        max_total_per_area=100 * unit_registry("MgC / ha"),
        saturation_period=2 * unit_registry("yr"),
        phase_in_period=1 * unit_registry("yr"),
        phase_out_period=1 * unit_registry("yr"),
        start_year=2020,
        out_years=np.arange(1850, 2300 + 1),
        model="tmodel",
        scenario="tscenario",
        variable="tvariable",
        region="tregion",
    )


@pytest.mark.parametrize("tphase_in_period", (0, -1, -5))
def test_create_pathway_raise_phase_in_period_lte_zero(tphase_in_period, tkwargs):
    tphase_in_period = tphase_in_period * unit_registry("yr")
    tkwargs["phase_in_period"] = tphase_in_period

    error_msg = re.escape("`phase_in_period` must be greater than or equal to 1 year")
    with pytest.raises(ValueError, match=error_msg):
        create_landuse_pathway(**tkwargs)


@pytest.mark.parametrize("tphase_out_period", (0, -1, -5))
def test_create_pathway_raise_phase_out_period_lte_zero(tphase_out_period, tkwargs):
    tphase_out_period = tphase_out_period * unit_registry("yr")
    tkwargs["phase_out_period"] = tphase_out_period

    error_msg = re.escape("`phase_out_period` must be greater than or equal to 1 year")
    with pytest.raises(ValueError, match=error_msg):
        create_landuse_pathway(**tkwargs)


@pytest.mark.parametrize("tsaturation_period", (0, -1, -5))
def test_create_pathway_raise_saturation_period_lte_zero(tsaturation_period, tkwargs):
    tsaturation_period = tsaturation_period * unit_registry("yr")
    tkwargs["saturation_period"] = tsaturation_period

    error_msg = re.escape("`saturation_period` must be greater than or equal to 1 year")
    with pytest.raises(ValueError, match=error_msg):
        create_landuse_pathway(**tkwargs)


@pytest.mark.parametrize(
    "bad_parameter,bad_units",
    (
        ("area", "m"),
        ("max_rate_per_area", "GtC / yr"),
        ("max_total_per_area", "GtC"),
        ("saturation_period", "kg"),
        ("phase_in_period", "m"),
        ("phase_out_period", "J"),
    ),
)
def test_bad_units(bad_parameter, bad_units, tkwargs):
    tkwargs[bad_parameter] = tkwargs[bad_parameter].magnitude * unit_registry(bad_units)
    with pytest.raises(pint.errors.DimensionalityError):
        create_landuse_pathway(**tkwargs)


@pytest.mark.parametrize("raise_if_greater_than_max", (True, False))
def test_exceeding_maximum_carbon(raise_if_greater_than_max):
    area = 1.0 * unit_registry("ha")
    max_rate_per_area = 1.0 * unit_registry("MgC / ha / yr")
    max_total_per_area = 100 * unit_registry("MgC / ha")

    saturation_period = 200 * unit_registry("yr")
    phase_in_period = 10 * unit_registry("yr")
    phase_out_period = 10 * unit_registry("yr")
    start_year = 1900
    out_years = np.arange(1850, 2300 + 1)

    model = "tmodel"
    scenario = "tscenario"
    variable = "tvariable"
    region = "tregion"

    tyunit = "tC / yr"
    txunit = "yr"
    exp_raw = _create_landuse_pathway_scmrun(
        yunit=tyunit,
        xunit=txunit,
        max_rate_per_area=max_rate_per_area,
        area=area,
        saturation_period=saturation_period,
        phase_in_period=phase_in_period,
        phase_out_period=phase_out_period,
        start_year=start_year,
        out_years=out_years,
        model=model,
        scenario=scenario,
        variable=variable,
        region=region,
    )

    max_uptake = max_total_per_area * area
    total_uptake = (
        float(
            exp_raw.integrate().convert_unit(str(max_uptake.units)).values.squeeze()[-1]
        )
        * max_uptake.units
    )

    def call():
        return create_landuse_pathway(
            area=area,
            max_rate_per_area=max_rate_per_area,
            max_total_per_area=max_total_per_area,
            saturation_period=saturation_period,
            phase_in_period=phase_in_period,
            phase_out_period=phase_out_period,
            start_year=start_year,
            out_years=out_years,
            model=model,
            scenario=scenario,
            variable=variable,
            region=region,
            raise_if_greater_than_max=raise_if_greater_than_max,
        )

    if raise_if_greater_than_max:
        error_msg = re.escape(
            "Maximum carbon to be taken up is {}. "
            "Carbon taken up according to pathway is {}.".format(
                max_uptake, total_uptake
            )
        )

        with pytest.raises(MaximumCarbonExceededError, match=error_msg):
            call()

    else:
        res = call().convert_unit(tyunit)

        excess_carbon = total_uptake - max_uptake
        required_shortening = excess_carbon / (max_rate_per_area * area)
        new_saturation_period = np.floor(saturation_period - required_shortening)

        exp = _create_landuse_pathway_scmrun(
            yunit=tyunit,
            xunit=txunit,
            max_rate_per_area=max_rate_per_area,
            area=area,
            saturation_period=new_saturation_period,
            phase_in_period=phase_in_period,
            phase_out_period=phase_out_period,
            start_year=start_year,
            out_years=out_years,
            model=model,
            scenario=scenario,
            variable=variable,
            region=region,
        )

        scmdt.assert_scmdf_almost_equal(res, exp)


@pytest.mark.parametrize(
    "saturation_period",
    (
        1 * unit_registry("yr"),
        10 * unit_registry("yr"),
    ),
)
def test_exceeding_maximum_carbon_cut_phase_periods_cut_both_periods(saturation_period):
    area = 1.0 * unit_registry("ha")
    max_rate_per_area = 1.0 * unit_registry("MgC / ha / yr")
    max_total_per_area = 1.0 * unit_registry("MgC / ha")
    phase_in_period = 2 * unit_registry("yr")
    phase_out_period = 2 * unit_registry("yr")
    start_year = 2020
    out_years = np.arange(1850, 2100 + 1)
    model = ("model",)
    scenario = ("scenario",)
    variable = ("variable",)
    region = ("region",)

    yunit = "MgC / yr"
    xunit = "yr"

    exp = _create_landuse_pathway_scmrun(
        yunit=yunit,
        xunit=xunit,
        max_rate_per_area=max_rate_per_area,
        area=area,
        saturation_period=saturation_period,
        phase_in_period=phase_in_period,
        phase_out_period=phase_out_period,
        start_year=start_year,
        out_years=out_years,
        model=model,
        scenario=scenario,
        variable=variable,
        region=region,
    ).timeseries(time_axis="year")
    # we cut the maximum production out so that the maximum carbon
    # uptake limit is obeyed
    exp.loc[:, 2021] = 0.5
    exp.loc[:, range(2022, out_years[-1])] = 0.0

    exp = scmdata.ScmRun(exp)

    res = create_landuse_pathway(
        area=area,
        max_rate_per_area=max_rate_per_area,
        max_total_per_area=max_total_per_area,
        saturation_period=saturation_period,
        phase_in_period=phase_in_period,
        phase_out_period=phase_out_period,
        start_year=start_year,
        out_years=out_years,
        model=model,
        scenario=scenario,
        variable=variable,
        region=region,
        raise_if_greater_than_max=False,
    )

    scmdt.assert_scmdf_almost_equal(res.convert_unit(yunit), exp)


@pytest.mark.parametrize(
    "saturation_period",
    (
        1 * unit_registry("yr"),
        10 * unit_registry("yr"),
    ),
)
def test_exceeding_maximum_carbon_cut_phase_periods_cut_both_periods_two_steps(
    saturation_period,
):
    area = 1.0 * unit_registry("ha")
    max_rate_per_area = 1.0 * unit_registry("MgC / ha / yr")
    max_total_per_area = 1.0 * unit_registry("MgC / ha")
    phase_in_period = 3 * unit_registry("yr")
    phase_out_period = 3 * unit_registry("yr")
    start_year = 2020
    out_years = np.arange(1850, 2100 + 1)
    model = ("model",)
    scenario = ("scenario",)
    variable = ("variable",)
    region = ("region",)

    yunit = "MgC / yr"
    xunit = "yr"

    exp = _create_landuse_pathway_scmrun(
        yunit=yunit,
        xunit=xunit,
        max_rate_per_area=max_rate_per_area,
        area=area,
        saturation_period=saturation_period,
        phase_in_period=phase_in_period,
        phase_out_period=phase_out_period,
        start_year=start_year,
        out_years=out_years,
        model=model,
        scenario=scenario,
        variable=variable,
        region=region,
    ).timeseries(time_axis="year")
    # we cut the maximum production out so that the maximum carbon
    # uptake limit is obeyed
    exp.loc[:, 2021] = exp.loc[:, 2021]
    exp.loc[:, range(2022, out_years[-1])] = 0.0

    exp = scmdata.ScmRun(exp)

    res = create_landuse_pathway(
        area=area,
        max_rate_per_area=max_rate_per_area,
        max_total_per_area=max_total_per_area,
        saturation_period=saturation_period,
        phase_in_period=phase_in_period,
        phase_out_period=phase_out_period,
        start_year=start_year,
        out_years=out_years,
        model=model,
        scenario=scenario,
        variable=variable,
        region=region,
        raise_if_greater_than_max=False,
    )

    scmdt.assert_scmdf_almost_equal(res.convert_unit(yunit), exp)


@pytest.mark.parametrize(
    "saturation_period",
    (
        1 * unit_registry("yr"),
        10 * unit_registry("yr"),
    ),
)
def test_exceeding_maximum_carbon_cut_phase_periods_cut_phase_out_more(
    saturation_period,
):
    area = 1.0 * unit_registry("ha")
    max_rate_per_area = 1.0 * unit_registry("MgC / ha / yr")
    max_total_per_area = 0.5 * unit_registry("MgC / ha")
    phase_in_period = 2 * unit_registry("yr")
    phase_out_period = 10 * unit_registry("yr")
    start_year = 2020
    out_years = np.arange(1850, 2100 + 1)
    model = ("model",)
    scenario = ("scenario",)
    variable = ("variable",)
    region = ("region",)

    yunit = "MgC / yr"
    xunit = "yr"

    exp = _create_landuse_pathway_scmrun(
        yunit=yunit,
        xunit=xunit,
        max_rate_per_area=max_rate_per_area,
        area=area,
        saturation_period=saturation_period,
        phase_in_period=phase_in_period,
        phase_out_period=phase_out_period,
        start_year=start_year,
        out_years=out_years,
        model=model,
        scenario=scenario,
        variable=variable,
        region=region,
    ).timeseries(time_axis="year")
    # the entire phase in period is cut, leaving only the end of the phase out period
    last_year = 2031 + saturation_period.to("yr").magnitude
    keep_period = range(last_year - 2, last_year + 1)
    exp.loc[:, range(2020, 2023)] = exp.loc[:, keep_period].values
    exp.loc[:, range(2023, out_years[-1])] = 0.0

    exp = scmdata.ScmRun(exp)

    res = create_landuse_pathway(
        area=area,
        max_rate_per_area=max_rate_per_area,
        max_total_per_area=max_total_per_area,
        saturation_period=saturation_period,
        phase_in_period=phase_in_period,
        phase_out_period=phase_out_period,
        start_year=start_year,
        out_years=out_years,
        model=model,
        scenario=scenario,
        variable=variable,
        region=region,
        raise_if_greater_than_max=False,
    )
    assert (res.filter(year=range(out_years[0], start_year)).values == 0).all()

    scmdt.assert_scmdf_almost_equal(res.convert_unit(yunit), exp)


@pytest.mark.parametrize(
    "phase_out_period,exp_raise",
    (
        (1000, True),
        (200, True),
        (199, True),
        (198, True),
        (197, False),
        (1, False),
    ),
)
def test_create_pathway_phase_out_year_gt_last_year(
    phase_out_period, exp_raise, tkwargs
):
    tkwargs["phase_in_period"] = 1 * unit_registry("yr")
    tkwargs["saturation_period"] = 1 * unit_registry("yr")
    tkwargs["phase_out_period"] = phase_out_period * unit_registry("yr")
    tkwargs["out_years"] = np.arange(1850, 2100 + 1)
    tkwargs["start_year"] = 1900

    if exp_raise:
        error_msg = re.escape(
            "The `phase_out_year` is {} which is greater than or equal to the last "
            "year in `out_years` which is {}, extend `out_years`".format(
                tkwargs["start_year"] + 1 + 1 + phase_out_period,
                tkwargs["out_years"][-1],
            )
        )
        with pytest.raises(ValueError, match=error_msg):
            create_landuse_pathway(**tkwargs)
    else:
        create_landuse_pathway(**tkwargs)


def test_rounding_edge_case():
    create_landuse_pathway(
        area = 108.445217 * unit_registry('megahectare'),
        max_rate_per_area = 1.19283209 * unit_registry('megagC / a / hectare'),
        max_total_per_area = 38.9084912 * unit_registry('megagC / hectare'),
        saturation_period = 41.8451006 * unit_registry('a'),
        phase_in_period = 25.0750121 * unit_registry('a'),
        phase_out_period = 104.964424 * unit_registry('a'),
        start_year = 2025,
        out_years = np.arange(2010, 4000 + 1, 1),
        raise_if_greater_than_max = False,
        model="model",
        scenario="scenario",
        variable="variable",
        region="region",
    )
