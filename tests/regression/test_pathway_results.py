import os.path

import json
import numpy as np
import pytest
import scmdata
import scmdata.testing as scmdt
from openscm_units import unit_registry

from one_earth.landuse_pathways import create_landuse_pathway


def test_pathways_results(update_expected_files, test_data_root_dir):
    specs_file = os.path.join(test_data_root_dir, "regression-pathway-specs.json")

    with open(specs_file, "r") as fh:
        configs = json.load(fh)

    if update_expected_files:
        updated_configs = []
    else:
        failures = []

    for cfg in configs:
        name = cfg.pop("name")
        exp = scmdata.ScmRun(
            data=cfg.pop("expected_values"),
            index=cfg["out_years"],
            columns=dict(
                model=cfg["model"],
                scenario=cfg["scenario"],
                region=cfg["region"],
                unit=cfg.pop("expected_unit"),
                variable=cfg["variable"],
            ),
        )

        inp = {}
        for k, v in cfg.items():
            if k.endswith("units"):
                continue

            unit_key = "{}-units".format(k)
            if unit_key in cfg:
                inp[k] = v * unit_registry(cfg[unit_key])
            else:
                inp[k] = v

        res = create_landuse_pathway(
            area=inp["area"],
            max_rate_per_area=inp["max_rate_per_area"],
            max_total_per_area=inp["max_total_per_area"],
            saturation_period=inp["saturation_period"],
            phase_in_period=inp["phase_in_period"],
            phase_out_period=inp["phase_out_period"],
            start_year=inp["start_year"],
            out_years=inp["out_years"],
            model=inp["model"],
            scenario=inp["scenario"],
            variable=inp["variable"],
            region=inp["region"],
        )

        if update_expected_files:
            tmp = {"name": name}
            for k, v in inp.items():
                if hasattr(v, "units"):
                    tmp[k] = v.magnitude
                    tmp["{}-{}".format(k, "units")] = str(v.units)
                elif isinstance(v, np.ndarray):
                    tmp[k] = [int(v) for v in v]
                else:
                    tmp[k] = v

            tmp["expected_values"] = [float(v) for v in res.values.squeeze()]
            tmp["expected_unit"] = res.get_unique_meta("unit", True)
            updated_configs.append(tmp)
        else:
            try:
                scmdt.assert_scmdf_almost_equal(res, exp)
            except AssertionError:
                failures.append(name)

    if update_expected_files:
        with open(specs_file, "w") as fh:
            json.dump(updated_configs, fh, indent=4)

        pytest.skip("Updated expected values")

    else:
        if failures:
            raise AssertionError("Failures: {}".format(failures))
