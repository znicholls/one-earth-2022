import os.path

import aneris
import pandas as pd
import pyam
import scmdata


SPECS_HANDLING_ID = "7d7c42"
UPTAKE_PATHWAYS_ID = "7d7c42"
SCENARIO_CREATION_ID = "7d7c42"
MAGICC_RUN_ID = "7d7c42"

HARMONISATION_YEAR = 2015
PREFIX = "One Earth 2021"

ROOT_DIR = os.path.join(os.path.dirname(__file__), "..")
DATA_DIR = os.path.join(ROOT_DIR, "data")
FIGURES_DIR = os.path.join(ROOT_DIR, "figures")

ANERIS_RC = aneris.RunControl(
    {
        "config": {
            "harmonize_year": HARMONISATION_YEAR,
            "replace_suffix": "Harmonised",
            "global_harmonization_only": False,
        },
        "prefix": PREFIX,
        "suffix": "Raw",
        "add_5regions": False,
    }
)

OVERRIDES_ANERIS = pd.DataFrame(
    columns=[
        "Model",
        "Scenario",
        "Region",
        "Variable",
        "Unit",
        "Method",
    ]
)

REGIONS_ANERIS = pd.DataFrame(
    [["World", "World", "World", "World"]],
    columns=[
        "ISO Code",
        "Country",
        "Native Region Code",
        "5_region",
    ],
)

ANERIS_KT_GASES = [
    "N2O",
    "SF6",
    "CF4",
    "C2F6",
    "C6F14",
    "HFC23",
    "HFC32",
    "HFC4310",
    "HFC4310mee",
    "HFC125",
    "HFC134a",
    "HFC143a",
    "HFC227ea",
    "HFC245fa",
    "CFC-11",
    "CFC-12",
    "CFC-113",
    "CFC-114",
    "CFC-115",
    "CH3CCl3",
    "CCl4",
    "HCFC-22",
    "HCFC-141b",
    "HCFC-142b",
    "Halon1211",
    "Halon1301",
    "Halon2402",
    "Halon1202",
    "CH3Br",
    "CH3Cl",
]


def rcmip_to_openscm_runner(inrun):
    def rename_variable(invar):
        if "F-Gases" in invar or "Montreal" in invar:
            bits = invar.split("|")
            out = "|".join([bits[0], bits[-1]])
        else:
            out = invar

        return out.replace("NMVOC", "VOC").replace("SOx", "Sulfur")

    out = inrun.copy()
    out["variable"] = out["variable"].apply(rename_variable)

    return out


def pymagicc_to_openscm_runner(inrun):
    def rename_variable(invar):
        return (
            invar.replace("NMVOC", "VOC")
            .replace("SOx", "Sulfur")
            .replace("HFC4310", "HFC4310mee")
        )

    out = inrun.copy()
    out["variable"] = out["variable"].apply(rename_variable)

    return out


def _mangle_scenario_name(idf, mangle_cols):
    out = idf.copy()

    for c in mangle_cols:
        out["scenario"] = out["scenario"] + "|" + out[c].astype(str)
    
    if isinstance(idf, scmdata.ScmRun):
        out = out.drop_meta(mangle_cols)
    else:
        out = out.drop(list(mangle_cols), axis="columns")

    return out


def _unmangle_scenario_name(idf, mangle_cols):
    out = idf.copy()

    for i, c in enumerate(mangle_cols):
        out[c] = out["scenario"].apply(lambda x: x.split("|")[i + 1])
    
    out["scenario"] = out["scenario"].apply(lambda x: x.split("|")[0])
    return out


_MANGLE_COLS = (
    "co2_landuse_baseline_model",
    "co2_landuse_baseline_scenario",
    "landuse_pathways_quantile",
    "nonco2_landuse_baseline_model",
    "nonco2_landuse_baseline_scenario",
)


def mangle_scenario_name(idf):
    return _mangle_scenario_name(idf, _MANGLE_COLS)


def unmangle_scenario_name(idf):
    return _unmangle_scenario_name(idf, _MANGLE_COLS)


_MANGLE_COLS_UPDATED_LABELLING = (
    "landuse_baseline",
    "energy_nonco2_pathway",
)


def mangle_scenario_name_from_updated_labelling(idf):
    return _mangle_scenario_name(idf, _MANGLE_COLS_UPDATED_LABELLING)


def unmangle_scenario_name_from_updated_labelling(idf):
    return _unmangle_scenario_name(idf, _MANGLE_COLS_UPDATED_LABELLING)


def update_labelling(inrun):
    out = inrun.copy()
    out["model"] = "not_relevant"
    out["scenario"] = "ECORES"
    out["landuse_baseline"] = out["co2_landuse_baseline_scenario"].map(
        {
            "SSP1-Baseline": "default",
            "End deforestation 2030": "default-2030",
            "End deforestation 2035": "default-2035",
            "End deforestation 2040": "default-2040",
            "End deforestation 2050": "default-2050",
        }
    )
    out["energy_nonco2_pathway"] = out["nonco2_landuse_baseline_scenario"].map(
        {
            "IMA15-TOT": "IMA15-TOT",
            "LowEnergyDemand": "LED",
            "S2": "UTS",
            "S2-delayed": "UTS-delayed",
        }
    )
    out["ecores_quantile"] = out["landuse_pathways_quantile"]
    
    drop_cols = [
        "co2_landuse_baseline_scenario",
        "co2_landuse_baseline_model",
        "nonco2_landuse_baseline_model",
        "nonco2_landuse_baseline_scenario",
        "landuse_pathways_quantile",
    ]
    
    if isinstance(out, scmdata.ScmRun):
        out = out.drop_meta(drop_cols)
    else:
        out = out.drop(list(drop_cols), axis="columns")
        
    return out


def annotate_ax(
    ax,
    text,
    xy,
    xycoords,
    alpha=0.4,
    facecolor="white",
    edgecolor="white",
):
    t = ax.annotate(
        text,
        xy=xy,
        xycoords=xycoords,
    )
    t.set_bbox(dict(alpha=alpha, facecolor=facecolor, edgecolor=edgecolor))

    return t

            
def set_legend_linewidths(leg, linewidth):
    for l in leg.get_lines():
        l.set_linewidth(linewidth)

    return leg


def download_sr15_scenario_explorer_output(out_path, out_name="sr15-scenario-explorer-output.csv"):
    conn = pyam.iiasa.Connection()
    print("Connecting to SR1.5 database")
    conn.connect("iamc15")

    print("Querying data")

    variables_to_download = (
        "*Emissions|CO2|AFOLU",
        "Carbon Sequestration|Land Use*",
    )
    out = []
    for v in variables_to_download:
        print(f"Downloading {v}")
        df = pyam.read_iiasa("iamc15", variable=v, region="World", meta=["category"])

        meta = df.meta.drop("exclude", axis="columns")
        df_with_meta = meta.join(df.timeseries().reset_index().set_index(meta.index.names))
        df_scmrun = scmdata.ScmRun(df_with_meta)
        out.append(df_scmrun)

    out = scmdata.run_append(out)

    os.makedirs(out_path, exist_ok=True)
    out.to_csv(os.path.join(out_path, out_name))
